//console.log("Hello World!");

//Array in programming is simply a list of data.

let studentA = '2020=1923';
let studentB = '2020-1924';
let studentC = '2020=1925';
let studentD = '2020-1926';
let studentE = '2020=1927';

let studentNumber = ['2020=1923', '2020-1924', '2020=1925', '2020-1926', '2020=1927'];

//Arrays
/*
	Arrays are used to store multiple values in a single variable
	They are declared using square brackets ([]) also known as "Array Literals"

	Syntax:

	let/const arrayName = [elementA, elementB, elementC...]
*/

//Common Example of Arrays

let grades = [98.5, 94.2, 89.2, 90.1];
let computerBrands = ['acer', 'asus', 'lenovo', 'neo', 'redfox', 'gateway', 'toshiba', 'fujitsu'];
console.log(grades);
console.log(computerBrands);

//Possible use of an array but it is not recommended

let mixedAr = [12, 'asus', null, undefined, {}];
console.log(mixedAr);

//Alternative way to write arrays

let myTasks = [
	'drink html',
	'eat javascript',
	'inhale css',
	'bake sass'
];
console.log(myTasks);

//mini-activity

let tasks = ['cleaning', 'take a bath', 'work', 'eating', 'sleeping'];
let capitalCities = ['Manila', 'Tokyo', 'Seoul', "Jerusalem"];
console.log(tasks);
console.log(capitalCities);

let city1 = "Tokyo";
let city2 =  "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities);

//length property

//The .length property allows us to get and set the total number of items in an arrya.

console.log(myTasks.length); //4
console.log(cities.length); //3

let blankarr = [];
console.log(blankarr.length); //0

//length property can also be used with strings
//some array methods and properties can also be used with strings

let fullName = 'Jamie Noble';
console.log(fullName.length); //11 including the space

//length property can also set he total number of items in our array, meaning we can actually delete the last item in the array or shorten the array by simply updating the length property of an array
myTasks.length = myTasks.length - 2;
console.log(myTasks.length); //2

//can also delete last item using decrement
cities.length--;
console.log(cities);

//Length of the strings are not affected
fullName.length = fullName.length - 1;
console.log(fullName.length);

fullName.length--;
console.log(fullName);

//adding an item to array
let theBeatles = ["John", "Paul", "Ringo", "George"];
//theBeatles.length++;

theBeatles[4]="Cardo";
theBeatles[theBeatles.length] = "Ely";
theBeatles[theBeatles.length] = "Chito";
theBeatles[theBeatles.length] = "MJ";
console.log(theBeatles);

//mini-activity
/*function addTrainers (trainer){
	theTrainers[theTrainers.length]=trainer;
}
addTrainers("Brock");
addTrainers("Misty");
console.log(theTrainers);
*/

//reading from Arrays
console.log(grades[0]); //98.5
console.log(computerBrands[3]); //neo
//accessing an array element that does not exist will return undefined
console.log(grades[20]);//undefined because wala pang ina-assign na value

let lakersLegends = ["kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);

let currentLaker = lakersLegends[2];
console.log(currentLaker);
console.log(currentLaker);


//Reassigning item from an array
console.log("Array before reassignment");
console.log(lakersLegends);
lakersLegends[2]="Pau Gasol";
console.log(lakersLegends);

//mini-activity
function findBlackMamba (arg){
	return lakersLegends[0];
}
let blackMamba = findBlackMamba(0);
console.log(blackMamba);


let favoriteFoods = [
"Tonkatsu",
"Adobo",
"Hamburger",
"Sinigang",
"Pizza"

];
console.log(favoriteFoods);

favoriteFoods[3]="Lasagna";
favoriteFoods[4]="Kilawin";
console.log(favoriteFoods);

//Accessing the last element of an array

let bullsLegends = ["Jordan", "Pipen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegends.length-1;
console.log(lastElementIndex); //4
console.log(bullsLegends.length); //5

console.log(bullsLegends[lastElementIndex]); //kukoc
console.log(bullsLegends[bullsLegends.length-1]); //kukoc

//Adding items into Array

let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);

//newArr[newArr.length-1] = "Aerith Gainsborough";

newArr[newArr.length] = "Barret Wallace";
console.log(newArr);

//Looping over an array

for (let index=0; index<newArr.length; index++){
	console.log(newArr[index]);
}

let numArr = [5, 12, 30, 46, 40];

for (let index = 0; index < numArr.length; index++){
	if (numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	} else {
		console.log(numArr[index] + " is not divisible by 5");
	}
}

//Multi-dimensional Array

/*
	-are useful for storing complex data structures
	-a practical application of this is to help visualize or create real world objects
*/

let chessBoard = [

	['a1','b1','c1','d1','e1','f1','g1','h1'],
	['a2','b2','c2','d2','e2','f2','g2','h2'],
	['a3','b3','c3','d3','e3','f3','g3','h3'],
	['a4','b4','c4','d4','e4','f4','g4','h4'],
	['a5','b5','c5','d5','e5','f5','g5','h5'],
	['a6','b6','c6','d6','e6','f6','g6','h6'],
	['a7','b7','c7','d7','e7','f7','g7','h7'],
	['a8','b8','c8','d8','e8','f8','g8','h8']
];
console.log(chessBoard);

//How to access elements of multi-dimensional array

console.log(chessBoard[1]);
console.log(chessBoard[1][4]);
console.log("Pawn moves to: " + chessBoard[1][5]);

//Mini-Activity

//access a8 and h6
console.log("Pawn moves to: " + chessBoard[7][0]);
console.log("Pawn moves to: " + chessBoard[5][7]);
